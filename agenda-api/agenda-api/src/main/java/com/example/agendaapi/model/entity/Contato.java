package com.example.agendaapi.model.entity;

import lombok.Data;


import javax.persistence.*;

@Entity
@Data

public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String nome;

    @Column
    private String email;

    @Column
    private Boolean favorito;



}
