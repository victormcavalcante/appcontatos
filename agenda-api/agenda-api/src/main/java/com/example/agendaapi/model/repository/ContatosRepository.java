package com.example.agendaapi.model.repository;

import com.example.agendaapi.model.entity.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContatosRepository extends JpaRepository<Contato, Integer> {
}
