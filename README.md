# App de Contatos


## Descrição e Funcionalidades

O objetivo do projeto era da criação de um CRUD básico, nas tecnologias SpringBoot, Angular(foi utilizado do angular-CLI para a criaçao da base do projeto) e como banco foi feito uso do PostgreSQL. A aplicação em si, se trata de um web app que serve para salvar contatos, por nome e email, onde esse usuário pode, também ser favoritado ou não, após cadastro. O usuário também pode ser alterado ou excluido de forma bem simples e funcional, sem muita dificuldade para encontrar essa funcionalidade. Adicionar um usuário ou alterá-lo, so sera valido caso os campos sejam preenchidos corretamente.

## Favoritar Contato

A ideia de favoritar contato seria mais como uma atualização parcial do usuário.

## Instalação

Deve-se ter o PostGreSQl instalado na sua máquina e dentro da máquina. Crie uma database no Postgres chamado "contato". Editar suas credenciais do Postgres em resources/application.properties
```
git clone https://gitlab.com/victormcavalcante/appcontatos.git

```

## Executar

Executar a Api no java mesmo, eclipse ou intellij
Dentro da pasta do agenda-app rodar o comando no terminal:

```
ng serve
```

acessar navegador localhost:4200

### Criar Contato
* Clicar em `novo`
* Preencher os campos requiridos
* Clicar em Adicionar

### Atualizar Contato

* Clicar em cima do usuario que deseja alterar (na linha que ele se encontra na lista)
* Um pop-up ira aparecer, com os campos que podem ser alterados (o Id não pode ser alterado)
* Alterar o campo que deseja
* Clicar em Alterar

### Excluir Contato

* Na linha que o usuário se encontra tem uma lixeira para exclui-lo

### Favoritar Contato

* Clicar no coração na linha que o usuário se encontra.

### Ver Contato

* Clicar em `Todos` para ver todos os contatos cadastrados 
