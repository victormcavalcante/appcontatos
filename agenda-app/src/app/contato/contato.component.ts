import { Component, OnInit } from '@angular/core';
import { ContatoService } from '../contato.service';
import { Contato } from './contato';
import { ContatoDetalheComponent } from '../contato-detalhe/contato-detalhe.component'

import {FormBuilder, FormGroup, Validators } from '@angular/forms'
import { MatSnackBar } from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog'



@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.css']
})
export class ContatoComponent implements OnInit {

  formulario!: FormGroup;
  contatos: Contato[] = [];
  colunas = ['id' , 'nome', 'email', 'actions']
  contatoSelecionado!: Contato;

  constructor(
    private service: ContatoService,
    private fb : FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.montarForm();
    this.listarContatos();
  }
  montarForm(){
    this.formulario = this.fb.group({
      nome: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]

    }) 
  }
  favoritarContato(contato: Contato){
    this.service.favourite(contato).subscribe(response =>{
      contato.favorito = !contato.favorito
    })
    
  }
  listarContatos(){
    this.service.list().subscribe(response => {
      this.contatos = response;
    })
  }
  visualizarContato(contato: Contato){
    this.dialog.open(ContatoDetalheComponent, {
      width: '500px',
      height: '320px',
      data: contato,
    } )
    this.dialog.afterAllClosed.subscribe(()=>{this.listarContatos()})
  }
  excluirContato(contato: Contato){
    this.service.delete(contato).subscribe(()=>{this.listarContatos()});
  }

  submit(){
    const formValues = this.formulario.value;
    const contato : Contato = new Contato(formValues.nome, formValues.email)
    this.service.save(contato).subscribe((resposta => {
      let lista : Contato[] = [...this.contatos, resposta]
      this.contatos = lista;
      this.snackBar.open('Contato adicionado com sucesso!','Sucesso', {duration:2000})
      this.formulario.reset();
      
    }))
  }

 }


