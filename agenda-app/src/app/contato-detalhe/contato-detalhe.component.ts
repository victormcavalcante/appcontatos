import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import { ContatoService } from '../contato.service';
import { Contato } from '../contato/contato';

@Component({
  selector: 'app-contato-detalhe',
  templateUrl: './contato-detalhe.component.html',
  styleUrls: ['./contato-detalhe.component.css']
})
export class ContatoDetalheComponent implements OnInit {
  formulario!: FormGroup;
  
  constructor(
    public dialogRef : MatDialogRef<ContatoDetalheComponent>,
    @Inject(MAT_DIALOG_DATA)public contato : Contato,
    private fb : FormBuilder,
    private service: ContatoService,
  ) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      id: [this.contato.id, Validators.required],
      nome: [this.contato.nome, Validators.required],
      email: [this.contato.email, [Validators.required, Validators.email]]
    })
    this.formulario.get('id')?.disable()
  }

  submit(){
    if(this.formulario.valid){
      this.service.edit(this.formulario.getRawValue()).subscribe(()=>{this.fechar()})

    }

  }  
  fechar(){
    this.dialogRef.close();
  }
}
